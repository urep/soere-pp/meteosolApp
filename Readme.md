# meteosolApp

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

Application Shiny pour Traiter/Visualiser/Travailler les données en sortie de capteurs météorologiques.

Application en cours de développement : [http://138.102.159.20:3838/meteosolApp/](http://138.102.159.20:3838/meteosolApp/)

## Installation

``` r
devtools::install_gitlab("urep/soere-pp/meteosolApp",host="https://forgemia.inra.fr")
```

## Deployer l'application

### En local

``` r
meteosolApp::run_app()
```

### Docker

A venir, test sur Docker avec shinyproxy