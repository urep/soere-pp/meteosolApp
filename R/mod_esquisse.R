#' mod_esquisseUI
#'
#' @description Module to build chart with esquisse
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS moduleServer reactiveValues
#' @import esquisse
#' 

mod_esquisseUI <- function(id){

	ns <- NS(id)
	esquisse::esquisserUI(id = ns("esquisse"),header = FALSE,choose_data = FALSE)
}

#' mod_timetrend Server Function
#'
#' @noRd 

mod_esquisse <- function(id,dataSelected){
moduleServer(
id,
function(input, output, session){

	ns <- session$ns

  # renderUI for esquisse module
  data_r <- reactiveValues(data=iris,name="dataUREP")
  
  observe({
      data_r$data <- dataSelected()
  })

  result <- callModule(module = esquisse::esquisserServer,id = "esquisse",data = data_r)
}
)
}

