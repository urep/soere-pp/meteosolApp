#' mod_timetrendUI
#'
#' @description Module to check timetrend of compile data
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom golem get_golem_options
#' @importFrom shiny NS tagList moduleServer checkboxGroupInput column fileInput reactiveValues
#' @importFrom shiny fluidRow updateCheckboxGroupInput isolate need observe observeEvent reactive h2 h5 icon validate 
#' @importFrom shinyBS bsButton
#' @importFrom DT dataTableOutput datatable renderDataTable formatDate
#' @importFrom data.table setkeyv setDT
#' @importFrom reshape melt
#' @importFrom wesanderson wes_palette
#' @import dygraphs
#' @import shinydashboard
#' @import meteosolToolbox
#' @importFrom utils read.csv
#' 

mod_timetrendUI <- function(id){

	ns <- NS(id)
	fluidRow(
     		box(width=12,title = tagList(shiny::icon("chart-bar"), "Time trend of variables selected"),
        			solidHeader = TRUE,collapsible = TRUE,status="info",collapsed=FALSE,
        			dygraphOutput(ns("dygraph"))
        		),
     		box(width=12,title = tagList(shiny::icon("table"), "Data of variables selected"),
        			solidHeader = TRUE,collapsible = TRUE,status="info",collapsed=FALSE,
        			dataTableOutput(ns("dataTable"))
        		)
        	)
}

#' mod_timetrend Server Function
#'
#' @noRd 

mod_timetrend <- function(id,dataSelected,frequenceSelected){
moduleServer(
id,
function(input, output, session){

	ns <- session$ns

  variableSelected <- reactive({
    as.character(unique(dataSelected()[,variable]))
  })

	# Data table selected with variable
	output$dataTable <- DT::renderDataTable({
      meltDataCompile <- dataSelected()
      meltDataCompile <- unique(meltDataCompile[TIMESTAMP >= values$start & TIMESTAMP <= values$end & variable %in% variableSelected(),])
      retData <- DT::datatable(meltDataCompile,rownames= FALSE,filter = 'top') %>%
			 	formatDate('TIMESTAMP',method = "toLocaleString",params = list("se", list(timeZone = "Africa/Algiers")))
  	  return(retData)
	})
	
	  #https://stackoverflow.com/questions/39859013/extract-dyrangeselector-values-from-dygraph-in-shiny-app
  	#https://stackoverflow.com/questions/30955578/r-shiny-dygraphs-series-reacting-to-date-window
  	values <- reactiveValues()  
  		observeEvent(input$dygraph_date_window,{
  			if (!is.null(input$dygraph_date_window)){
    		start <- input$dygraph_date_window[[1]]
    		end <- input$dygraph_date_window[[2]]
    		values$start <- as.POSIXct(start,"%Y-%m-%dT%H:%M:%S",tz='Africa/Algiers')
    		values$end <- as.POSIXct(end,"%Y-%m-%dT%H:%M:%S",tz='Africa/Algiers')
    	}else{}
  	})

	# dygraph of variables selected
	output$dygraph <- renderDygraph({
      print("dygraph")
      validate(need(!is.null(dataSelected()),"Please choose a file to upload"))
      print(head(dataSelected()))
      print(variableSelected())
      print(frequenceSelected())
      dataXTS <- dataToXts2(dataSelected(),variableSelected(),frequenceSelected())
			
      #dataXTS <- dataToXts2(rawFile,"perm_5_R1_INT_Avg","day")
      
      if(grepl("year",frequenceSelected())){
        axisYears<-"function(d){ return d.getFullYear() }"
        tickerYears <- "function(a, b, pixels, opts, dygraph, vals) { 
                          return Dygraph.getDateAxis(a, b, Dygraph.ANNUAL, opts, dygraph)}"
        DataTimezone <- FALSE
      }else{
        axisYears<-NULL
        tickerYears <- NULL
        DataTimezone <- TRUE
      }
      #@source https://stackoverflow.com/questions/33885817/r-dygraphs-x-axis-granularity-from-monthly-to-yearly
      dygraphs::dygraph(dataXTS,width="100%") %>%
               dyAxis("x", axisLabelFormatter=axisYears,ticker= tickerYears) %>%
               dyOptions(stackedGraph = FALSE) %>%
               dyRangeSelector(height = 20,strokeColor = "") %>%  
               dyLegend(show = "onmouseover") %>% 
               dyRangeSelector(retainDateWindow=TRUE) %>%
               dyHighlight(highlightSeriesBackgroundAlpha = 0.8,highlightSeriesOpts = list(strokeWidth = 3)) %>%   
               dyOptions(colors = wes_palette("Zissou1", length(variableSelected()),type = "continuous"),retainDateWindow=TRUE,useDataTimezone=TRUE) 
	})
}
)
}

