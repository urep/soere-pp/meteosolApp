#' The application server-side
#' 
#' @param input,output,session Internal parameters for {shiny}. 
#'     DO NOT REMOVE.
#' @noRd

app_server <- function( input, output, session ) {
  # List the first level callModules here
  	
  	rv <- reactiveValues(dataSelected = NULL,frequenceSelected = NULL)
	
	observe({
    	rv$dataSelected <- userDataSelected$dataSelected
    	rv$frequenceSelected <- userDataSelected$frequenceSelected
	})

  	userCheckFile <- reactive({
	    req(input$checkFile)
  	})
  	
  	output$menu <- renderMenu({
		sidebarMenu(id="tabs",
                menuItem("Compilation",tabName = "compilation",icon=icon("cog")),
                menuItem("Check compile data", icon = icon("table"),startExpanded = TRUE,
                         menuSubItem("csv file", tabName = "checkCompileFile",selected=TRUE),
                         menuSubItem("xlsx file", tabName = "xlsx")
                ),
                menuItem("Download", icon=icon("cog"),
                         menuSubItem("ACBB-Format", tabName = "ACBB")
                )
      )
  	})

	userDataSelected <- mod_loadData("mod_loadDataUI_1",rawFilePath=userCheckFile)
	mod_timetrend("mod_timetrendUI_1",dataSelected=rv$dataSelected,frequenceSelected=rv$frequenceSelected)
	mod_stats("mod_statsUI_1",dataSelected = rv$dataSelected)
	mod_esquisse("mod_esquisseUI_1",dataSelected = rv$dataSelected)

}


